// Registering component 
AFRAME.registerComponent('graphboundary', {
    dependencies: ['graphpoint'],
    schema: {
        color: { type: 'color', default: '#1ffd84' },
        length: { type: 'number', default: 0.5 },
        radii_size: {type: 'number', default: 0.005},
        point: {type: 'vec2', default: {x:0.1, y:0.14}}
    },
    init: function () {
        console.log("init started...");
        
        var leverMaterial = new THREE.MeshBasicMaterial({color: this.data.color });
//                                           CylinderGeometry(radius_at_one_end, radius_at_other_end, lenght, radial_segments[edges to make up radius'])
        var track = new THREE.Mesh(new THREE.CylinderGeometry(this.data.radii_size,this.data.radii_size, this.data.size), leverMaterial, 6);
        var track2 = new THREE.Mesh(new THREE.CylinderGeometry(this.data.radii_size,this.data.radii_size, this.data.size), leverMaterial, 6);
        var track3 = new THREE.Mesh(new THREE.CylinderGeometry(this.data.radii_size,this.data.radii_size, this.data.size), leverMaterial, 6);
        track.rotateX(Math.PI / 2);
        track3.rotateZ(Math.PI / 2);

        var origin = new THREE.Mesh(new THREE.SphereGeometry(this.data.radii_size * 15,6,6), leverMaterial, 6);  

        var chassis = new THREE.Group();
        chassis.add(track);
        chassis.add(track2);
        chassis.add(track3);
        chassis.add(origin);

        var pointEl = document.createElement('a-entity');
        pointEl.setAttribute('graphpoint', 'color: #ffffff')
        this.el.appendChild(pointEl)
        
        pointEl.setAttribute('position', { x: this.data.point.x, y: this.data.point.y, z: pointEl.getAttribute('position').z });

        console.log("about to add: ", chassis);

        this.el.setObject3D('mesh', chassis);

        console.log("...init done!");
    },
   update: function () {},
   tick: function () {},
   remove: function () {},
   pause: function () {},
   play: function () {}
  });



  // Registering component 
AFRAME.registerComponent('graphpoint', {
    schema: {
        color: { type: 'color', default: '#8ff084' },
       // point: {type: 'vec2', default: {x:0.1, y:0.14}},
    },
    init: function () {
        var pointMaterial = new THREE.MeshBasicMaterial({color: this.data.color });
        var pointSphere = new THREE.Mesh(new THREE.SphereGeometry(.0005 * 10,6,6), pointMaterial, 6);
        // pointSphere.translateX( this.data.point.x);
        // pointSphere.translateY( this.data.point.y);

        this.el.setObject3D('mesh', pointSphere);

    },
   update: function () {},
   tick: function () {},
   remove: function () {},
   pause: function () {},
   play: function () {}
  });